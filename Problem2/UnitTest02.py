import unittest
import Problem02

class TestUM(unittest.TestCase):
    def setUp(self):
        pass

    def test_confision01(self):
        self.assertEqual(Problem02.lstConfision([1,2,3,5,7],[1,2,4,8]), [1,2])

    def test_confision02(self):
        self.assertEqual(Problem02.lstConfision([1, 2, 3, 5, 7], []), [])

    def test_confision03(self):
        self.assertEqual(Problem02.lstConfision([1, 2, 3, 5, 7]), [])

    def test_confision04(self):
        self.assertEqual(Problem02.lstConfision(), [])

    def test_confision05(self):
        self.assertEqual(Problem02.lstConfision([-1, -2, -3, 5, 7], [1,2,3,5,6]), [5])

    def test_confision06(self):
        self.assertEqual(Problem02.lstConfision("String", [1,2,3,5,6]), [])

    def test_confision07(self):
        self.assertEqual(Problem02.lstConfision('A', 'B'), [])

if __name__ == '__main__':
    unittest.main()

