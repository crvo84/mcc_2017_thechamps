import random
def problem02():

    print("List Confusion")

    a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 101]
    b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,101]

    print("List a")
    print(a)

    print("List b")
    print(b)

    finalList01 = []

    for i in set(a):
        if i in b:
            finalList01.append(i)

    print("Final List 01")
    print(finalList01)


    #Extras
    print("\n/Extras/")

    randomA = random.sample(range(0,100), random.randint(1,49))
    randomB = random.sample(range(0,100), random.randint(1,49))

    print("Random List A")
    print(randomA)
    print("Random List B")
    print(randomB)

    finalList02 = [i for i in set(randomA) if i in randomB]


    print("Final List 02")
    print(finalList02)

problem02()