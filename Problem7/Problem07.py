def listDuplicates(*lists):
    if (len(lists) != 2):
	    return None

    if (lists[0] == None or lists[1] == None):
        return None

    if (not isinstance(lists[0], list) or not isinstance(lists[1], list)):
        return None

    resultList = []

    listA = lists[0]
    listB = lists[1]

    for i in range(len(listA)):
        if listA[i] in listB:
            resultList.append(listA[i])

    return resultList

# print listDuplicates([1,2,3,4,5,6,7,8,9,2,4,6,8], [0])
# print listDuplicates([1,2,3,4,5,6,7,8,9,2,4,6,8], [1,2,3,4,5,6,7,8,9,8,7,6,5,4,3,2,1])
# print listDuplicates()
# print listDuplicates([])
# print listDuplicates([], 5)
