import unittest
import Problem07

class TestUM(unittest.TestCase):

	def setup(self):
		pass

	def test_listDuplicates01(self):
		self.assertEqual(Problem07.listDuplicates([1,2,3,4,5,6,7,8,9,2,4,6,8], [0]), [])

	def test_listDuplicates02(self):
		self.assertEqual(Problem07.listDuplicates([1,2,3,4,5,6,7,8,9,2,4,6,8], [1,2,3,4,5,6,7,8,9,8,7,6,5,4,3,2,1]), [1, 2, 3, 4, 5, 6, 7, 8, 9, 2, 4, 6, 8])

	def test_listDuplicates03(self):
		self.assertEqual(Problem07.listDuplicates(), None)

	def test_listDuplicates04(self):
		self.assertEqual(Problem07.listDuplicates([]), None)

	def test_listDuplicates05(self):
		self.assertEqual(Problem07.listDuplicates([], 5), None)		

		
if __name__ == '__main__':
	unittest.main()
