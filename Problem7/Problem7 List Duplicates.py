def problem7(inputLst):
    lst = inputLst
    resultLst = []

    for i in range(len(lst)):
        if lst[i] not in resultLst:
            resultLst.append(lst[i])

    print(resultLst)


lst01 = [1,2,3,4,5,6,7,8,9,2,4,6,8]
lst02 = [0]
lst03 = [1,2,3,4,5,6,7,8,9,8,7,6,5,4,3,2,1]

print("Lst01:")
print(lst01)
print("Solution:")
problem7(lst01)
print("=================")
print("Lst02:")
print(lst02)
print("Solution:")
problem7(lst02)
print("=================")
print("Lst03:")
print(lst03)
print("Solution:")
problem7(lst03)

