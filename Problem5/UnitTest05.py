import unittest
import Problem05

class TestUM(unittest.TestCase):

	def test_reverse01(self):
		self.assertEqual(Problem05.reverse(), None)

	def test_reverse02(self):
		self.assertEqual(Problem05.reverse(None), None)
		self.assertNotEqual(Problem05.reverse(None), "")

	def test_reverse03(self):
		self.assertEqual(Problem05.reverse(1000), None)
		self.assertNotEqual(Problem05.reverse(1), "")

	def test_reverse04(self):
		self.assertEqual(Problem05.reverse(3.1416), None)
		self.assertNotEqual(Problem05.reverse(3.1416), "")

	def test_reverse05(self):
		self.assertEqual(Problem05.reverse(False), None)

	def test_reverse06(self):
		self.assertEqual(Problem05.reverse([]), None)

	def test_reverse07(self):
		self.assertEqual(Problem05.reverse(""), "")

	def test_reverse08(self):
		self.assertEqual(Problem05.reverse("hola como estas"), "estas como hola ")

if __name__ == '__main__':
    unittest.main()