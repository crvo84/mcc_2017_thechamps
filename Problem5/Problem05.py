
def reverse(*str):
    if (len(str) == 0):
        return None

    if (str[0] == None):
        return None

    if (isinstance(str[0], (int, float, bool, list))):
        return None

    result = ""

    strTest = str[0]

    strTest = strTest.split()
    strTest = strTest[::-1]

    for i in strTest:
        result += i
        result += ' '

    return result


def problem05():
    user_input = input("Enter a string to reverse\n")

    result = ""

    user_input = user_input.split()
    user_input = user_input[::-1]

    for i in user_input:
        result += i
        result += ' '

    print(result)

# print reverse()
# print reverse(None)
# print reverse(1000)
# print reverse(2.3)
# print reverse(False)
# print reverse([])
# print reverse("")
# print reverse("hola como estas")



