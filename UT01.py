import unittest
from Problem6 import problem6

class TestUM(unittest.TestCase):
    def setUp(self):
        pass

    def test_cows01(self):
        self.assertEqual(problem6(1234,1234), [4,0])

    def test_cows02(self):
        self.assertNotEqual(problem6(1234,4321), [4,0])


if __name__ == '__main__':
    unittest.main()
