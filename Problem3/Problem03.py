import collections

def isPalindrome(*str):
    if (len(str) == 0):
        return False

    if (str[0] == None):
        return False

    if (isinstance(str[0], (int, float, bool, list))):
        return False

    return str[0] == (str[0])[::-1]



def problem03():

    print("List Analysis")
    print("You can try with:")
    print("0 - is a palindrome")
    print("alola - is a palindrome")
    print("lalala - is not a palindrome")

    user_input = input("Enter a string\n")


    if (user_input == (user_input[::-1])):
        print("%s is a palindrome" %user_input)
    else:
        print("%s is not a palindrome" % user_input)




# problem03()

# print isPalindrome("alola")
# print isPalindrome("hola")
# print isPalindrome("")
# print isPalindrome()
# print isPalindrome(False)
# print isPalindrome([])
# print isPalindrome(500)

