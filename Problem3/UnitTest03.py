import unittest
import Problem03

class TestUM(unittest.TestCase):

	def setup(self):
		pass

	def test_isPalindrome01(self):
		self.assertEqual(Problem03.isPalindrome("alola"), True)
		self.assertEqual(Problem03.isPalindrome("anitalavalatina"), True)
		self.assertEqual(Problem03.isPalindrome(""), True)

	def test_isPalindrome02(self):
		self.assertNotEqual(Problem03.isPalindrome("hola"), True)
		self.assertNotEqual(Problem03.isPalindrome("1"), False)

	def test_isPalindrome03(self):
		self.assertEqual(Problem03.isPalindrome(False), False)
		self.assertEqual(Problem03.isPalindrome(True), False)

	def test_isPalindrome04(self):
		self.assertNotEqual(Problem03.isPalindrome([]), True)

	def test_isPalindrome05(self):
		self.assertEqual(Problem03.isPalindrome(), False)

	def test_isPalindrome06(self):
		self.assertNotEqual(Problem03.isPalindrome(500), True)

	def test_isPalindrome07(self):
		self.assertEqual(Problem03.isPalindrome(500.2), False)
		
if __name__ == '__main__':
	unittest.main()