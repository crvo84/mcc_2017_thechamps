import unittest
import Problem04

class TestUM(unittest.TestCase):
	def test_compareFiles01(self):
		self.assertEqual(Problem04.compareFiles(), None)
	def test_compareFiles02(self):
		self.assertEqual(Problem04.compareFiles("PrimeNumbers.txt"), 1)
	def test_compareFiles03(self):
		self.assertEqual(Problem04.compareFiles("PrimeNumbers.txt","HappyNumbers.txt"), 1)
	def test_compareFiles03(self):
		self.assertEqual(Problem04.compareFiles("PrimeNumbers.txt","HappyNumbers.txt","algo.txt"), None)

