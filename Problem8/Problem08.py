def checklistallnumbers(a):
    for i in range(len(a)):
        if not isinstance(i,int): return False
    return True

def problem8(lst):
    for i in range(len(lst)):
        if not checklistallnumbers(lst[i]): return -1
    size = len(lst[0])

    if size == 1:
        return (lst[0][0])
    elif size == 2:
        return ((lst[0][0]*lst[1][1]) - (lst[0][1]*lst[1][0]))
    else:
        result = (lst[0][0]*lst[1][1]*lst[2][2]) + (lst[0][1]*lst[1][2]*lst[2][0]) + (lst[0][2]*lst[1][0]*lst[2][1]) - (lst[0][2]*lst[1][1]*lst[2][0]) - (lst[0][1]*lst[1][0]*lst[2][2]) - (lst[0][0]*lst[1][2]*lst[2][1])
        return result



lst01 = [[1]]
lst02 = [[2,3],
         [-1,2]]
lst03 = [[3,2,1],
         [0,2,-5],
         [-2,1,4]]

print("lst01 = 1x1:")
print(lst01)
print("Determinant: %i" %problem8(lst01))
print("=================")
print("lst02 = 2x2:")
print(lst02)
print("Determinant: %i" %problem8(lst02))
print("=================")
print("lst03 = 3x3:")
print(lst03)
print("Determinant: %i" %problem8(lst03))