import unittest
import Problem08

class TestUM(unittest.TestCase):
    def setUp(self):
        pass

    def test_determinant01(self):
        lst01 = [[1]]
        self.assertEqual(Problem08.problem8(lst01),1)

    def test_determinant02(self):
        lst02 = [[2, 3],[-1, 2]]
        self.assertEqual(Problem08.problem8(lst02),7)

    def test_determinant03(self):
        lst03 = [[3, 2, 1],[0, 2, -5],[-2, 1, 4]]
        self.assertEqual(Problem08.problem8(lst03),63)

    def test_determinant04(self):
        lst02 = [[2, 3],[-1, 2]]
        self.assertNotEquals(Problem08.problem8(lst02),12)

    def test_determinant05(self):
        lst02 = [[2, 3],[-1, 2]]
        self.assertNotEquals(Problem08.problem8(lst02),-1)




if __name__ == '__main__':
    unittest.main()

