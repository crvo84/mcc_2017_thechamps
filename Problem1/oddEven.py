import random

def isEven(number):
    return number % 2 == 0

def isMultipleOf4(number):
    return number % 4 == 0

def gcd(firstNum, secondNum):
    return firstNum % secondNum == 0

def isPrime(number):
    if number < 1: return False
    for i in range(2,number):
        if number % i == 0:
            return False
    return True


def problem01():
    print("Odd or Even")
    print("You can try with:")
    print("0 - is an even number")
    print("12 - is an even number")
    print("13 - is an odd number")


    user_input = int(input("Enter a number\n"))

    if isEven(user_input):
        print("%d is an even number" % user_input)
    else:
        print("%d is an odd number" % user_input)

    # Extras
    print("\n/Extras/")

    if isMultipleOf4(user_input):
        print("is a multiple of 4")
    else:
        print("is not a multiple of 4")

    if isPrime(user_input):
        print("is a prime number")
    else:
        print("is not a prime number")

    firstNumber = int(input("Enter a number\n"))
    secondNumber = int(input("Enter another number\n"))

    if gcd(firstNumber,secondNumber):
        print("%d is factor of %d" %(secondNumber,firstNumber))
    else:
        print("%d is not factor of %d" %(secondNumber,firstNumber))



problem01()