import random
import sys

def specialCases(number):

    if (number == None): return False
    if (not isinstance(number, int)): return False
    if (isinstance(number, bool)): return False
    if (number > sys.maxsize or number < -sys.maxsize - 1): return False
    return True

def isZero(number):
    return number == 0

def noArguments(*number):
    return len(number) == 0

def twoArguments(*number):
    return len(number) != 2

def isEven(*number):
    if noArguments(*number): return False
    if not specialCases(number[0]): return False
    return number[0] % 2 == 0

#Extra
def isMultipleOf4(*number):
    if noArguments(*number): return  False
    if not specialCases(number[0]): return False
    return number[0] % 4 == 0

def gcd(*twoNum):
    if (twoArguments(*twoNum)): return False
    if not specialCases(twoNum[0]): return False
    if not specialCases(twoNum[1]): return False
    if twoNum[0] < 1 or twoNum[1] < 1: return False
    if isZero(twoNum[1]): return False
    return twoNum[0] % twoNum[1] == 0

def isPrime(*number):
    if noArguments(*number): return False
    if not specialCases(number[0]): return False
    if number[0] < 2: return False
    for i in range(2,number[0]):
        if number[0] % i == 0:
            return False
    return True


def problem01():
    print("Odd or Even")
    print("You can try with:")
    print("0 - is an even number")
    print("12 - is an even number")
    print("13 - is an odd number")


    user_input = int(input("Enter a number\n"))

    if isEven(user_input):
        print("%d is an even number" % user_input)
    else:
        print("%d is an odd number" % user_input)

    # Extras
    print("\n/Extras/")

    if isMultipleOf4(user_input):
        print("is a multiple of 4")
    else:
        print("is not a multiple of 4")

    if isPrime(user_input):
        print("is a prime number")
    else:
        print("is not a prime number")

    firstNumber = int(input("Enter a number\n"))
    secondNumber = int(input("Enter another number\n"))

    if gcd(firstNumber,secondNumber):
        print("%d is factor of %d" %(secondNumber,firstNumber))
    else:
        print("%d is not factor of %d" %(secondNumber,firstNumber))


print(gcd(24,12))

