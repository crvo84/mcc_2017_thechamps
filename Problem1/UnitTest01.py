import unittest
import Problem01

class TestUM(unittest.TestCase):
    def setUp(self):
        pass

    def test_isEven(self):
        #Equal
        self.assertEqual(Problem01.isEven(2), True)
        self.assertEqual(Problem01.isEven(0), True)
        self.assertEqual(Problem01.isEven(-2), True)
        #Not Equal
        self.assertNotEqual(Problem01.isEven(-1), True)
        self.assertNotEqual(Problem01.isEven(1), True)
        self.assertNotEqual(Problem01.isEven(10000000000000000098321), True)
        self.assertNotEqual(Problem01.isEven(-10000000000000000098321), True)
        self.assertNotEqual(Problem01.isEven(None), True)
        self.assertNotEqual(Problem01.isEven("String"), True)
        self.assertNotEqual(Problem01.isEven('C'), True)
        self.assertNotEqual(Problem01.isEven([]), True)
        self.assertNotEqual(Problem01.isEven(False), True)
        self.assertNotEqual(Problem01.isEven(), True)

    def test_isMultipleOf4(self):
        # Equal
        self.assertEqual(Problem01.isMultipleOf4(4), True)
        self.assertEqual(Problem01.isMultipleOf4(0), True)
        self.assertEqual(Problem01.isMultipleOf4(-4), True)
        # Not Equal
        self.assertNotEqual(Problem01.isMultipleOf4(-1), True)
        self.assertNotEqual(Problem01.isMultipleOf4(1), True)
        self.assertNotEqual(Problem01.isMultipleOf4(10000000000000000098321), True)
        self.assertNotEqual(Problem01.isMultipleOf4(-10000000000000000098321), True)
        self.assertNotEqual(Problem01.isMultipleOf4(None), True)
        self.assertNotEqual(Problem01.isMultipleOf4("String"), True)
        self.assertNotEqual(Problem01.isMultipleOf4('C'), True)
        self.assertNotEqual(Problem01.isMultipleOf4([]), True)
        self.assertNotEqual(Problem01.isMultipleOf4(False), True)
        self.assertNotEqual(Problem01.isMultipleOf4(), True)

    def test_gcd(self):
        # Equal
        self.assertEqual(Problem01.gcd(24,12), True)
        # Not Equal
        self.assertNotEqual(Problem01.gcd(-24, -12), True)
        self.assertNotEqual(Problem01.gcd(24, -12), True)
        self.assertNotEqual(Problem01.gcd(-24, 12), True)
        self.assertNotEqual(Problem01.gcd(0, 0), True)
        self.assertNotEqual(Problem01.gcd(10000000000000000098321, 10000000000000000098321), True)
        self.assertNotEqual(Problem01.gcd(-10000000000000000098321, -10000000000000000098321), True)
        self.assertNotEqual(Problem01.gcd(10000000000000000098321, -10000000000000000098321), True)
        self.assertNotEqual(Problem01.gcd(-10000000000000000098321, 10000000000000000098321), True)
        self.assertNotEqual(Problem01.gcd(12, None), True)
        self.assertNotEqual(Problem01.gcd(None, 12), True)
        self.assertNotEqual(Problem01.gcd(None, None), True)
        self.assertNotEqual(Problem01.gcd("String", "String"), True)
        self.assertNotEqual(Problem01.gcd('C', 'C'), True)
        self.assertNotEqual(Problem01.gcd([], []), True)
        self.assertNotEqual(Problem01.gcd(True, False), True)
        self.assertNotEqual(Problem01.gcd(), True)

    def test_isPrime(self):
        #Equal
        self.assertEqual(Problem01.isPrime(7), True)
        self.assertEqual(Problem01.isPrime(23), True)
        #Not Equal
        self.assertNotEqual(Problem01.isPrime(-1), True)
        self.assertNotEqual(Problem01.isPrime(1), True)
        self.assertNotEqual(Problem01.isPrime(0), True)
        self.assertNotEqual(Problem01.isPrime(4), True)
        self.assertNotEqual(Problem01.isPrime(10000000000000000098321), True)
        self.assertNotEqual(Problem01.isPrime(-10000000000000000098321), True)
        self.assertNotEqual(Problem01.isPrime(None), True)
        self.assertNotEqual(Problem01.isPrime("String"), True)
        self.assertNotEqual(Problem01.isPrime('C'), True)
        self.assertNotEqual(Problem01.isPrime([]), True)
        self.assertNotEqual(Problem01.isPrime(False), True)
        self.assertNotEqual(Problem01.isPrime(), True)



if __name__ == '__main__':
    unittest.main()

