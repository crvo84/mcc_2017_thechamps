import sys

def checkLstAllNumbers(a):
    for i in range(len(a)):
        if not isinstance(i,int): return False
    return True

def twoArguments(*number):
    return len(number) == 2

def specialCases(number):

    if (number == None): return False
    if (not isinstance(number, int)): return False
    if (isinstance(number, bool)): return False
    if (number > sys.maxsize or number < -sys.maxsize - 1): return False
    return True

def binary_search(*lst):

    if not twoArguments(*lst): return False

    if not checkLstAllNumbers(lst[0]): return False

    if not specialCases(lst[1]): return False

    return binary_search_aux(lst[0], lst[1], 0, len(lst[0])-1)

def binary_search_aux(lst,n,start,end):

    if (start > end):
        return False

    mid = (int)((start+end)/2)

    if lst[mid] == n:
        return True
    elif n < lst[mid]:
        return binary_search_aux(lst, n, start, mid-1)
    elif n > lst[mid]:
        return binary_search_aux(lst, n, mid+1, end)


lst01 = [1,2,3,4,5,6,7,8,9]
lst02 = [1,2,3,4,5,6,7,8,9]
lst03 = [1,2,3,4,5,6,7,8,9]

print("Lst01:")
print(lst01)
print("Target Value = 6")
print(binary_search(lst01, 6))
print("=================")
print("Lst02:")
print(lst02)
print("Target Value = 0")
print(binary_search(lst01, 0))
print("=================")
print("Lst03:")
print(lst03)
print("Target Value = 12")
print(binary_search(lst01, 12))