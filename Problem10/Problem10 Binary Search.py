def binary_search(lst, n):
    return binary_search_aux(lst, n, 0, len(lst)-1)

def binary_search_aux(lst,n,start,end):

    if (start > end):
        return False

    mid = (int)((start+end)/2)

    if lst[mid] == n:
        return True
    elif n < lst[mid]:
        return binary_search_aux(lst, n, start, mid-1)
    elif n > lst[mid]:
        return binary_search_aux(lst, n, mid+1, end)


lst01 = [1,2,3,4,5,6,7,8,9]
lst02 = [1,2,3,4,5,6,7,8,9]
lst03 = [1,2,3,4,5,6,7,8,9]

print("Lst01:")
print(lst01)
print("Target Value = 6")
print(binary_search(lst01, 6))
print("=================")
print("Lst02:")
print(lst02)
print("Target Value = 0")
print(binary_search(lst01, 0))
print("=================")
print("Lst03:")
print(lst03)
print("Target Value = 12")
print(binary_search(lst01, 12))

