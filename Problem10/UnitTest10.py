import unittest
import Problem10

class TestUM(unittest.TestCase):
    def setUp(self):
        pass

    def test_binarySearch01(self):
        lst01 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        self.assertTrue(Problem10.binary_search(lst01,6))
        self.assertTrue(Problem10.binary_search(lst01, 1))
        self.assertTrue(Problem10.binary_search(lst01, 9))

    def test_binarySearch02(self):
        lst01 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        self.assertFalse(Problem10.binary_search(lst01,0))
        self.assertFalse(Problem10.binary_search(lst01, 10))
        self.assertFalse(Problem10.binary_search(lst01, -3))

    def test_binarySearch03(self):
        lst01 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        self.assertFalse(Problem10.binary_search())
        self.assertFalse(Problem10.binary_search(False))
        self.assertFalse(Problem10.binary_search(lst01, "String"))




if __name__ == '__main__':
    unittest.main()

