import unittest
import Problem06

class TestUM(unittest.TestCase):

	def setup(self):
		pass

	def test_compare_numbers01(self):
		self.assertEqual(Problem06.compare_numbers(1), None)

	def test_compare_numbers02(self):
		self.assertEqual(Problem06.compare_numbers(1, 2, 3), None)

	def test_compare_numbers03(self):
		self.assertEqual(Problem06.compare_numbers(None, 1), None)

	def test_compare_numbers04(self):
		self.assertEqual(Problem06.compare_numbers(1, None), None)

	def test_compare_numbers05(self):
		self.assertEqual(Problem06.compare_numbers(None, None), None)

	def test_compare_numbers06(self):
		self.assertEqual(Problem06.compare_numbers("hola", 1), None)

	def test_compare_numbers07(self):
		self.assertEqual(Problem06.compare_numbers(1.5, 1), None)

	def test_compare_numbers08(self):
		self.assertEqual(Problem06.compare_numbers(2, []), None)

	def test_compare_numbers09(self):
		self.assertEqual(Problem06.compare_numbers(1, False), None)

	def test_compare_numbers10(self):
		self.assertEqual(Problem06.compare_numbers("4271", "1234"), [1, 2])



		
if __name__ == '__main__':
	unittest.main()


# print compare_numbers(1) 
# print compare_numbers(1, 2, 3)
# print compare_numbers(None, 1)
# print compare_numbers(1, None)
# print compare_numbers(None, None)
# print compare_numbers("hola", 1)
# print compare_numbers(1.5, 1)
# print compare_numbers(2, [])
# print compare_numbers(1, False)
# print compare_numbers("4271", "1234")