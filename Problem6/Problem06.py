import random

def compare_numbers(*nums):
    if (len(nums) != 2):
        return None

    if (nums[0] == None or nums[1] == None):
        return None

    if (isinstance(nums[0], (int, float, bool, list)) or isinstance(nums[1], (int, float, bool, list))):
        return None

    cowbull = [0,0]

    number = nums[0]
    user_input = nums[1]

    for i in range(len(number)):
        for j in range(len(user_input)):
            if (number[i] == user_input[j]) and (i == j):
                cowbull[0] += 1
            elif (number[i] == user_input[j]):
                cowbull[1] += 1

    return cowbull


def problem6(number,user_input):

    #number = str(random.randint(0, 9999))
    print(number)

    #user_input = str(random.randint(0, 9999))

    cowbull = compare_numbers(str(number), str(user_input))

    print("%i Cows and %i Bulls" % (cowbull[0], cowbull[1]) )


# print compare_numbers(1) 
# print compare_numbers(1, 2, 3)
# print compare_numbers(None, 1)
# print compare_numbers(1, None)
# print compare_numbers(None, None)
# print compare_numbers("hola", 1)
# print compare_numbers(1.5, 1)
# print compare_numbers(2, [])
# print compare_numbers(1, False)
# print compare_numbers("4271", "1234")

