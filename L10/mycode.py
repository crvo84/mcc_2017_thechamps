"""
Some simple functions that we will use in our tests.
"""

import math

def concatenateStrings(strA, strB):
    return strA + strB
    
def standardDeviation(nums):
    avg = average(nums)
    sum = 0
    for num in nums:
        sum += (num - avg) * (num - avg)
    dividedSum = sum/len(nums)
    return math.sqrt(dividedSum)

def average(nums):
    sum = 0.0
    for num in nums:
        sum += num
    return sum/len(nums)

def sortNums(nums):
    return sorted(nums)
    

# print "-----------------------"
#
# print "Concatenate strings:"
# print "strA + strB = " + concatenateStrings("strA", "strB")
#
# print "-----------------------"
#
# print "Standard deviation [1, 2, 3, 4, 5] = "
# print standardDeviation([1, 2, 3, 4, 5])
#
# print "-----------------------"
#
# print "Sort array [4, 5, 7, 1, 3, 2, 6, 0] = "
# print sortNums([4, 5, 7, 1, 3, 2, 6, 0])
#
# print "-----------------------"