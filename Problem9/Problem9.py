import csv
import math

class Auto:

    id = 0
    mgp = 0
    cylinders = 0
    displacement = 0
    horsepower = 0
    weight = 0
    acceleration = 0
    year = 0
    origin = 0

    def __init__(self, id, mpg, cylinders, displacement, horsepower, weight, acceleration, year, origin, name):
        self.id = id
        self.name = name
        self.mpg = mpg
        self.cylinders = cylinders
        self.displacement = displacement
        self.horsepower = horsepower
        self.weight = weight
        self.acceleration = acceleration
        self.year = year
        self.origin = origin

def population_mean(lst):

    result = 0

    for i in lst:
        result += i
    return result/len(lst)

def standard_deviation(lst):
    result = 0
    mean = population_mean(lst)

    for i in lst:
        result += pow((i - mean),2)

    return result/len(lst)

def mode(lst):
    return max(set(lst), key=lst.count)


def bubbleSort(nlist):
    for passnum in range(len(nlist)-1,0,-1):
        for i in range(passnum):
            if nlist[i]>nlist[i+1]:
                temp = nlist[i]
                nlist[i] = nlist[i+1]
                nlist[i+1] = temp


def quartile(lst):
    QLst = []

    ln = len(lst)

    lst.sort()

    for i in range(len(lst)):
        if i == int(ln/4):
            QLst.append(lst[i])
            continue
        if i == int(ln/2):
            QLst.append(lst[i])
            continue
        if i == int(3*ln/4):
            QLst.append(lst[i])
            continue

    return QLst


def linear_regression(lst01, lst02):
    print("Linear Regression - MPG vs Horsepower")

    xi = population_mean(lst01)
    yi = population_mean(lst02)

    xi2 = 0
    yi2 = 0

    cxy = 0

    for i in range(len(lst01)):
        cxy += lst01[i] * lst02[i]

    cxy /= len(lst01)
    cxy -= xi * yi

    cx2 = 0
    cy2 = 0

    for i in range(len(lst01)):
        cx2 += lst01[i] * lst01[i]
        cy2 += lst02[i] * lst02[i]

    cx2 /= len(lst01)
    cy2 /= len(lst01)

    cx2 -= xi * xi
    cy2 -= yi * yi

    print("y = %d x + %d" %((cxy/cx2), ((cxy/cx2)-xi+yi)))
    print("x = %d y + %d" %((cxy/cy2), ((cxy/cy2)+xi-yi)))

    parameters = []
    parameters.append(xi)
    parameters.append(yi)
    parameters.append(cxy)
    parameters.append(cx2)
    parameters.append(cy2)

    return parameters


autoLst = []
mpgLst = []
cylindersLst = []
displacementLst = []
horsepowerLst = []
weightLst = []
accelerationLst = []
yearLst = []
originLst = []

with open('Auto.csv', 'rb') as csvfile:
    f = csv.reader(csvfile, delimiter=',')
    firstline = True
    for row in f:
        if firstline:  # skip first line
            firstline = False
            continue
        tmp = Auto(int(row[0]), float(row[1]), int(row[2]), float(row[3]), int(row[4]), int(row[5]), float(row[6]), int(row[7]), int(row[8]), row[9])
        autoLst.append(tmp)


for i in autoLst:

    mpgLst.append(i.mpg)
    cylindersLst.append(i.cylinders)
    displacementLst.append(i.displacement)
    horsepowerLst.append(i.horsepower)
    weightLst.append(i.weight)
    accelerationLst.append(i.acceleration)
    yearLst.append(i.year)
    originLst.append(i.origin)


print("=====Auto.csv data set=====")

print("1) Population Mean")

print("Mpg: %d"             %population_mean(mpgLst))
print("Cylinders: %d"       %population_mean(cylindersLst))
print("Displacement: %d"    %population_mean(displacementLst))
print("Horsepower: %d"      %population_mean(horsepowerLst))
print("Weight: %d"          %population_mean(weightLst))
print("Acceleration: %d"    %population_mean(accelerationLst))
print("Year: %d"            %population_mean(yearLst))
print("Origin: %d"          %population_mean(originLst))


print("===============================================")

print("2) Standard Deviation")

print("Mpg: %d"             %standard_deviation(mpgLst))
print("Cylinders: %d"       %standard_deviation(cylindersLst))
print("Displacement: %d"    %standard_deviation(displacementLst))
print("Horsepower: %d"      %standard_deviation(horsepowerLst))
print("Weight: %d"          %standard_deviation(weightLst))
print("Acceleration: %d"    %standard_deviation(accelerationLst))
print("Year: %d"            %standard_deviation(yearLst))
print("Origin: %d"          %standard_deviation(originLst))


print("===============================================")

print("3) Mode")

print("Mpg: %d"             %mode(mpgLst))
print("Cylinders: %d"       %mode(cylindersLst))
print("Displacement: %d"    %mode(displacementLst))
print("Horsepower: %d"      %mode(horsepowerLst))
print("Weight: %d"          %mode(weightLst))
print("Acceleration: %d"    %mode(accelerationLst))
print("Year: %d"            %mode(yearLst))
print("Origin: %d"          %mode(originLst))

print("===============================================")

print("4) Quartiles")

print("Mpg: Q1 - %d, Q2 - %d, Q3 - %d, "             %(quartile(mpgLst)[0], quartile(mpgLst)[1], quartile(mpgLst)[2] ))
print("Cylinders: Q1 - %d, Q2 - %d, Q3 - %d, "       %(quartile(cylindersLst)[0], quartile(cylindersLst)[1], quartile(cylindersLst)[2] ))
print("Displacement: Q1 - %d, Q2 - %d, Q3 - %d, "    %(quartile(displacementLst)[0], quartile(displacementLst)[1], quartile(displacementLst)[2] ))
print("Horsepower: Q1 - %d, Q2 - %d, Q3 - %d, "      %(quartile(horsepowerLst)[0], quartile(horsepowerLst)[1], quartile(horsepowerLst)[2] ))
print("Weight: Q1 - %d, Q2 - %d, Q3 - %d, "          %(quartile(weightLst)[0], quartile(weightLst)[1], quartile(weightLst)[2] ))
print("Acceleration: Q1 - %d, Q2 - %d, Q3 - %d, "    %(quartile(accelerationLst)[0], quartile(accelerationLst)[1], quartile(accelerationLst)[2] ))
print("Year: Q1 - %d, Q2 - %d, Q3 - %d, "            %(quartile(yearLst)[0], quartile(yearLst)[1], quartile(yearLst)[2] ))
print("Origin: Q1 - %d, Q2 - %d, Q3 - %d, "          %(quartile(originLst)[0], quartile(originLst)[1], quartile(originLst)[2] ))


print("===============================================")

print("5) Linear Regression")
print(linear_regression(mpgLst, horsepowerLst))




